package com.thetestroom.cucumber;

import org.junit.BeforeClass;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks {

    @Before
    public void setup(){

        WebDriverUtils.getNewChromeDriver();
    }

    @After
    public void tearDown(Scenario scenario){
//        if (!scenario.isFailed())
        {
            WebDriverUtils.closeDriver();
        }
    }
}
